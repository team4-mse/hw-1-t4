package com.example.hw_1_t_4;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
//import android.widget.LinearLayout;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.Arrays;
import java.util.ArrayList;



public class MainActivity extends AppCompatActivity {
    ImageButton closeBtn;
    Button submitBtn;
    com.google.android.material.floatingactionbutton.FloatingActionButton version;

    boolean isRotate = false;

    EditText outputTextBox;
    EditText outputErrorBox;
    String str;
    String outputString;
    EditText userRawInput;
    String lengthError = "Array is out of bounds. User Input must be between 3-8 digits";
    String digitError = "Each digit must have a value between 0-9";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        closeBtn = (ImageButton) findViewById(R.id.imageButton2);
        closeBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                finish();
                System.exit(0);
            }
        });

        version = (FloatingActionButton) findViewById(R.id.floatingActionButton2);
        version.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                isRotate = rotateFab(view, !isRotate);
                Snackbar.make(view, "© 2020 BubbleSortlyBook.io | Version: 2.0", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
            }
        });

        // initialize gui outputTextBox
        outputTextBox = (EditText) findViewById(R.id.textViewOutput);
        outputTextBox.setFocusableInTouchMode(false);
        outputTextBox.clearFocus();

        // initialize gui outputErrorBox
        outputErrorBox = (EditText) findViewById(R.id.textViewError);

        // initialize gui vars
        submitBtn = (Button) findViewById(R.id.button);
        userRawInput = (EditText) findViewById(R.id.editTextNumber);

        // change input font size
        //userRawInput.setTextSize(35);

        // change submit button and text color
        submitBtn.setBackgroundColor(Color.parseColor("#B7FEF7"));
        submitBtn.setTextColor(Color.parseColor("#000000"));


        //submitBtn.setWidth(6000);
        //submitBtn.setLayoutParams(new LinearLayout.LayoutParams(5, 5));



        // trigger validation when submit button is clicked
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer[] finalIntArr = validate(userRawInput.getText().toString());
                if (finalIntArr == null){
                    return;
                }
                formatArrayForView(bubbleSort(finalIntArr));
            }
        });
    }


    protected Integer[] validate(String str) {
        // Reset output and error box
        clearOutputBox();

        // Hide keyboard
        hideDisplayKeyboard();

        //Clean up spacing in user input
        String userInput = str.replaceAll("\\s+", " ");
        userInput = userInput.trim();

        // Init arrays
        String[] usrStrArr = userInput.split(" ");
        Integer[] usrIntArr = new Integer[usrStrArr.length];

        try {
            for (int i = 0; i < usrStrArr.length; i++) {
                usrIntArr[i] = Integer.parseInt(usrStrArr[i]);
            }
        } catch (NumberFormatException | NullPointerException e){
            return null;
        }

        //Check the array is within range
        if (usrIntArr.length < 3 || usrIntArr.length > 8) {

            // Call error box to display error string
            displayError(lengthError);

            Log.d("ERROR", lengthError);
            Log.d("ERROR", Arrays.toString(usrIntArr));
            Log.d("ERROR", String.valueOf(usrIntArr.length));
            return null;


        }

        //Check integers in array are within limit
        for (Integer i : usrIntArr) {
            if (i < 0 || i > 9) {

                // Call error box to display error string
                displayError(digitError);

                Log.d("ERROR", digitError);
                Log.d("ERROR", String.valueOf(i));
                return null;
            }
        }

        return usrIntArr;
    }

    public static ArrayList<ArrayList<String>> bubbleSort(Integer arr[])
    {
        int temp;
        int numValues = arr.length;
        String iterCharCheck = "";
        String tempIt = "";

        ArrayList<ArrayList<String>> outer = new ArrayList<ArrayList<String>>();
        ArrayList<String> inner1 = new ArrayList<String>();

        inner1.add(Arrays.toString(arr));
        for (int current = 0; current < numValues-1; current++)
        {
            for (int j = numValues - 1; j > current; j--)
            {
                if (arr[j] < arr[j-1])
                {
                    tempIt = arr[j].toString();
                    temp = arr[j-1];
                    arr[j-1] = arr[j];
                    arr[j] = temp;
                }
                if (iterCharCheck != tempIt) {
                    if (!iterCharCheck.isEmpty()) {
                        outer.add(inner1);
                        String itBegin = inner1.get(inner1.size() - 1);
                        inner1 = new ArrayList<String>();
                        inner1.add(itBegin);
                    }
                    iterCharCheck = tempIt;
                    Log.d("temp", "test" + tempIt);
                }
                String itEnd = inner1.get(inner1.size() - 1);
                if (!itEnd.equals(Arrays.toString(arr))) {
                    inner1.add(Arrays.toString(arr));
                }
            }
        }
        outer.add(inner1);
        return outer;
    }


    // There are a couple ways I can go here.  I can have a 2d array where the the itItem would
    // contain an array of strings that I just displayed (like "12345")
    // or a 3d array as is the current function below where the it item would contain an array of
    // numbers that I would iterate through.  Either one is fine, I think it should be be whatever
    // is easiest for Sazeda.
    protected  void formatArrayForView(ArrayList<ArrayList<String>> arr) {
        ArrayList<String> lastArray = arr.get(arr.size() - 1);
        outputString = "";
        outputString += "Input array " + '\n' + (arr.get(0).get(0)) + "\n\n";
        outputString += "Final Sort " + '\n' + (lastArray.get(lastArray.size() - 1)) + "\n\n";
        outputString += "Intermediate Steps" + "\n\n";
        for (int iteration = 0; iteration < arr.size(); iteration++)
        {
            Log.d("STATE", arr.get(iteration).toString());
            outputString += "Iteration " + (iteration + 1) + '\n';
            for (int itItem = 0; itItem < arr.get(iteration).size(); itItem++)
            {
                outputString += String.valueOf(arr.get(iteration).get(itItem)) + "\n";
                if (itItem == arr.get(iteration).size() - 1) {
                    outputString += "\n";
                }
            }
        }
        outputTextBox.setText(str);
        outputTextBox.setText(outputString);
    }

    protected void clearOutputBox()
    {
        // Clear out all outputBox
        outputTextBox.setText("");
        outputErrorBox.setText("");
    }

    protected void displayError(String errString)
    {
        // Display error
        outputErrorBox.setText(errString);
    }

    protected void hideDisplayKeyboard()
    {
        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static boolean rotateFab(final View v, boolean rotate) {
        v.animate().setDuration(200)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                })
                .rotation(rotate ? 360f : 0f);
        return rotate;
    }
}

